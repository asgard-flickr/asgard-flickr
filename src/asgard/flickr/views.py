from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404
from django.core.paginator import Paginator, InvalidPage, EmptyPage

from asgard.flickr.models import Photo

def index(request, page=1, context={}, template_name='flickr/index.html'):
	photo_list = Photo.objects.all()
	paginator = Paginator(photo_list, 60)
	
	try:
		photos = paginator.page(page)
	except (EmptyPage, InvalidPage):
		photos = paginator.page(paginator.num_pages)
	
	context.update({
		'photos': photos,
		'is_archive': True,
	})
	
	return render_to_response(template_name, context, context_instance=RequestContext(request))

def detail(request, photo_id, context={}, template_name='flickr/detail.html'):
	try:
		photo = Photo.objects.get(photo_id=photo_id)
	except Photo.DoesNotExist:
		raise Http404
	
	context.update({
		'photo': photo,
	})
	
	return render_to_response(template_name, context, context_instance=RequestContext(request))