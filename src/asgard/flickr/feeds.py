from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.syndication.feeds import Feed, FeedDoesNotExist

from tagging.models import TaggedItem, Tag

from asgard.flickr.models import Photo

current_site = Site.objects.get_current()

class BaseFeed(Feed):
	subtitle = u"More than a hapax legomenon."
	title_description = 'feeds/flickr_photo_title.html'
	description_template = 'feeds/flickr_photo_description.html'
	
	def item_pubdate(self, item):
		return item.date_uploaded
	
	def item_updated(self, item):
		return item.date_updated
	
	def item_id(self, item):
		return item.get_absolute_url()
	
	def item_author_name(self, item):
		return item.taken_by
	
	def item_author_link(self, item):
		return item.flickr_profile_url
	
	def item_categories(self, item):
		return item.tag_set.all()
	
	def item_copyright(self, item):
		return item.license_code

class FlickrPhotoFeed(BaseFeed):
	title = u"%s: flickr photos." % current_site.name
	
	def link(self):
		return reverse('flickr_index') + "?utm_source=feedreader&utm_medium=feed&utm_campaign=FlickrPhotoFeed"
	
	def items(self):
		return Photo.objects.all()[:10]
	
	def item_link(self, item):
		return item.get_absolute_url() + "?utm_source=feedreader&utm_medium=feed&utm_campaign=FlickrPhotoFeed"