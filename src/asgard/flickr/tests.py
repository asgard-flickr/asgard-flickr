from django.test import Client
from django.test import TestCase
from django.core.urlresolvers import reverse

client = Client()

class FlickrTestCase(TestCase):
	fixtures = ['flickr',]
	
	def setUp(self):
		from asgard.flickr.models import Photo
		self.photo = Photo.objects.get(pk=1)
	
	def testFlickrIndex(self):
		response = client.get(reverse('flickr_index'))
		self.assertEquals(response.status_code, 200)
	
	def testPhotoDetailThoughModel(self):
		response = client.get(self.photo.get_absolute_url())
		self.assertEquals(response.status_code, 200)
	
	def testPhotoDetailThoughURL(self):
		response = client.get(reverse('flickr_photo_detail', args=[self.photo.photo_id]))
		self.assertEquals(response.status_code, 200)
	
	def testBlogSitemaps(self):
		response = client.get(reverse('sitemap'))
		self.assertEquals(response.status_code, 200)
	
	def testBlogPostFeed(self):
		response = client.get(reverse('feeds', args=['flickr']))
		self.assertEquals(response.status_code, 200)