from django.contrib.sitemaps import Sitemap

from asgard.flickr.models import Photo

class FlickrPhotosSitemap(Sitemap):
	changefreq = "never"
	priority = 1.0
	
	def items(self):
		return Photo.objects.all()
	
	def lastmod(self, obj):
		return obj.date_updated
