from django.conf.urls.defaults import *
from django.contrib import admin

admin.autodiscover()

from asgard.flickr.sitemaps import FlickrPhotosSitemap
from asgard.flickr.feeds import FlickrPhotoFeed

feeds = {
	'flickr': FlickrPhotoFeed,
}

sitemaps = {
	'flickr': FlickrPhotosSitemap,
}

urlpatterns = patterns('',
	(r'^admin/', include(admin.site.urls)),
	(r'^comments/', include('django.contrib.comments.urls')),
	
	(r'^flickr/', include('asgard.flickr.urls')),
	
	url(r'^feeds/(?P<url>.*)/$',
		'django.contrib.syndication.views.feed',
		{ 'feed_dict': feeds },
		name = 'feeds'
	),
	
	url(r'^sitemap.xml$',
		'django.contrib.sitemaps.views.sitemap',
		{ 'sitemaps': sitemaps },
		name = 'sitemap'
	),
)