from django.conf.urls.defaults import *

urlpatterns = patterns('asgard.flickr.views',
	url(r'^page/(?P<page>\d+)/$',
		view = 'index',
		name = 'flickr_index_paginated',
	),
	url(r'^(?P<photo_id>\w+)/$',
		view = 'detail',
		name = 'flickr_photo_detail',
	),
	url(r'^$',
		view = 'index',
		name = 'flickr_index',
	),
)