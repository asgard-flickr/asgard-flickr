from django.contrib import admin

from asgard.flickr.models import Photo

class PhotoAdmin(admin.ModelAdmin):
	list_display = ('admin_thumbnail', 'title', 'date_uploaded',)
	date_hierarchy = 'date_uploaded'

admin.site.register(Photo, PhotoAdmin)